package com.noddysoft.wordly.service;

import java.util.Optional;

import com.noddysoft.wordly.model.Word;

public interface WordService {

	void addSingleWord(String text);
	Iterable<Word> getAllWords();
	Optional<Word> getWordById(Long id);

}