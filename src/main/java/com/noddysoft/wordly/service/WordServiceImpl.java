package com.noddysoft.wordly.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.noddysoft.wordly.model.Word;
import com.noddysoft.wordly.repository.WordRepository;

@Service
public class WordServiceImpl implements WordService {
	
	@Autowired
	private WordRepository wordRepository;
	
	public void addSingleWord(String text) {
		Word word = new Word();
		word.setText(text);
		word.setId(getNextAvailableWordId());
		
		wordRepository.save(word);	
	}
	
	private Long getNextAvailableWordId() {
		//The word-table counts from 0 - the count is the next available number.
		return wordRepository.count();
	}
	
	public Iterable<Word> getAllWords() {
		return wordRepository.findAll();
	}
	
	public Optional<Word> getWordById(Long id) {
		return wordRepository.findById(id);
	}
}
