package com.noddysoft.wordly.repository;

import org.springframework.data.repository.CrudRepository;

import com.noddysoft.wordly.model.Word;

public interface WordRepository extends CrudRepository<Word, Long> {
	
}
