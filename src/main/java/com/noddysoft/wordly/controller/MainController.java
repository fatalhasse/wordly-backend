package com.noddysoft.wordly.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.noddysoft.wordly.model.Word;
import com.noddysoft.wordly.service.WordService;

@Controller
@RequestMapping("/wordly")
public class MainController {
	
	@Autowired
	private WordService wordService;
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public @ResponseBody String addSingleWord (@RequestParam String text) {
		
		wordService.addSingleWord(text);
		return "Saved";
	}
	
	@RequestMapping(value="/all", method = RequestMethod.GET)
	public @ResponseBody Iterable<Word> getAllWords() {
		return wordService.getAllWords();
	}
	
	@RequestMapping(value="/single", method = RequestMethod.GET)
	public @ResponseBody Optional<Word> getSingleWordById(@RequestParam Long id) {
		return wordService.getWordById(id);
	}
	
	
}
